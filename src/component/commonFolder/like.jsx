import React, { Component } from "react";
//input given enity is liked or not ? Boolean
//output: RAISE ONCLICK EVENT TO TOGGLE THE LIGHT

const Like = props => {
  let classes = "fa fa-heart";
  if (!props.liked) classes += "-o";
  return (
    <i
      style={{ cursor: "pointer" }}
      onClick={props.onClick}
      className={classes}
      aria-hidden="true"
    />
  );
};

export default Like;

// class Like extends Component {
//   state = {};
//   render() {
//     let classes = "fa fa-heart";
//     if (!this.props.liked) classes += "-o";
//     return (
//       <i
//         style={{ cursor: "pointer" }}
//         onClick={this.props.onClick}
//         className={classes}
//         aria-hidden="true"
//       />
//     );
//   }
// }

// export default Like;
