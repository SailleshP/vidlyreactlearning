import React, { Component } from "react";
import Joi from "joi-browser";
import Form from "./commonFolder/form";

class LoginForm extends Form {
  // username = React.createRef();

  state = {
    data: { username: "", password: "" },
    errors: {
      // username:'Username is required.'
    }
  };

  schema = {
    username: Joi.string()
      .required()
      .label("User Name"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  // componentDidMount() {
  //   this.username.current.focus();
  // }

  doSubmit = () => {
    console.log("submittedf");
  };

  render() {
    const { data, errors } = this.state;

    //ctrl +shift+p  wrapping
    //cursoe alt key and multiple crusor change
    return (
      <div>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            {this.renderInput("username", "User Name", "text")}
            {this.renderInput("password", "Password", "password")}
          </div>
          {this.renderButton("Login")}
        </form>
      </div>
    );
  }
}

export default LoginForm;
